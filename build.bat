@echo off

echo Python 2.6.9 x86_64 build
set SCRIPT_DIR=%~dp0
set PY_DIR=%SCRIPT_DIR%\Python-2.6.9

set MSSdk=C:\Program Files\Microsoft SDKs\Windows\v7.0
set FxTools=C:\Windows\Microsoft.NET\Framework64\v3.5;C:\Windows\Microsoft.NET\Framework\v3.5
set SdkSetupDir=%MSSdk%\Setup
set SdkTools=%MSSdk%\Bin\x64;%MSSdk%\Bin
set VSRoot=C:\Program Files (x86)\Microsoft Visual Studio 9.0\
set VCRoot=%VSRoot%\VC\
set VCINSTALLDIR=%VCRoot%

set Include=%VCRoot%\Include;%MSSdk%\Include;%MSSdk%\Include\gl
set Lib=%VCRoot%\Lib\amd64;%MSSdk%\Lib\X64

set TARGETOS=WINNT
set CPU=AMD64
set NODEBUG=1
set PROCESSOR_ARCHITECTURE=AMD64

set DISTUTILS_USE_SDK=1
set HOST_PYTHON=%SCRIPT_DIR%\host_python\python.exe
set PERL_BIN=%SCRIPT_DIR%\perl\bin
set DIST=%SCRIPT_DIR%\pmi_dist

set Path=%VCRoot%\Bin\amd64;%VCRoot%\vcpackages;%VSRoot%\Common7\IDE;%SdkTools%;%FxTools%;%MSSdk%\Setup;%windir%\system32;%windir%;%windir%\System32\Wbem;%PERL_BIN%


echo Cleaning ...
if exist %DIST% ( rmdir /s /q %DIST% )
msbuild %PY_DIR%\PCbuild\pcbuild.sln /nologo /property:Configuration=Release /property:platform=x64 /clp:ErrorsOnly /target:clean

echo Building, see logs in 'msbuild1.log' ...
msbuild %PY_DIR%\PCbuild\pcbuild.sln /nologo /property:Configuration=Release /property:platform=x64 /clp:ErrorsOnly /fl1 /target:build 

echo Making dist ...
mkdir %DIST%
xcopy /q %PY_DIR%\PCBuild\amd64\python26.dll %DIST%
xcopy /q %PY_DIR%\PCBuild\amd64\python.exe %DIST%
xcopy /q %PY_DIR%\PCBuild\amd64\pythonw.exe %DIST%
xcopy /q %PY_DIR%\LICENSE %DIST%
xcopy /q %PY_DIR%\README %DIST%
mkdir %DIST%\DLLs
xcopy /q %PY_DIR%\PCBuild\amd64\*.pyd %DIST%\DLLs
xcopy /q %PY_DIR%\PCBuild\amd64\sqlite3.dll %DIST%\DLLs
xcopy /q %PY_DIR%\PC\py.ico %DIST%\DLLs
xcopy /q %PY_DIR%\PC\pyc.ico %DIST%\DLLs
mkdir %DIST%\include
xcopy /s /q %PY_DIR%\Include %DIST%\include
xcopy /q %PY_DIR%\PC\pyconfig.h %DIST%\include
mkdir %DIST%\Lib
xcopy /s /q /y %PY_DIR%\Lib %DIST%\Lib /exclude:%SCRIPT_DIR%\dist_excludes.txt
mkdir %DIST%\libs
xcopy /q %PY_DIR%\PCBuild\amd64\*.lib %DIST%\libs

echo Python 2.6.9 x86_64 build complete: %DIST% 


echo pywin32-b218 x86_64 build

echo Cleaning ...
if exist %SCRIPT_DIR%\pywin32-b218\build ( rmdir /s /q %SCRIPT_DIR%\pywin32-b218\build )

echo Building, see logs in 'pywin32.log' ...
%DIST%\python %SCRIPT_DIR%\pywin32-b218\setup.py install > pywin32.log 2>&1

echo Build complete
echo Press any key to close the window ...
pause > nul
